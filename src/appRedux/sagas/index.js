import {all} from "redux-saga/effects";
import authSagas from "./Auth";
import userSagas from "./UserSagas";

export default function* rootSaga(getState) {
  yield all([
    authSagas(),
    userSagas()
  ]);
}
