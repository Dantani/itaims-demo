import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import axios from "axios";
import { baseURL, baseURLJP } from "../../util/config";
import {
  GET_USER,
  GET_TODO,
  SAVE_USER_DATA,
  UPDATE_USER_DATA,
  DELETE_USER_DATA,
} from "../../constants/ActionTypes";
import { showErrorMessage, getUsersSuccess, getTodoSuccess } from "../actions/UserActions";
import { message } from "antd";
import { push } from "react-router-redux";

export const token = (state) => state.token;
let licenseId = "";
let langName = "";

const headers = {
  "Content-Type": "application/json",
};

/*user api call section start*/
const getUserAPICallRequest = async (payloadData) =>
  await axios
    .get(baseURLJP + '/users',{ headers: headers }
    )
    .then((getUserRes) => getUserRes.data)
    .catch((error) => error);

const getTodoAPICallRequest = async (payloadData) =>
  await axios
    .get(baseURLJP + '/todos?userId='+payloadData.userId,{ headers: headers }
    )
    .then((getTodoRes) => getTodoRes.data)
    .catch((error) => error);

const saveUserAPICallRequest = async (payloadData) =>
  await axios
    .post(baseURL + "/users", payloadData, {
      headers: headers,
    })
    .then((getSaveRes) => getSaveRes.data)
    .catch((error) => error);

const updateUserAPICallRequest = async (payloadData) =>
  await axios
    .put(baseURL + "/users/"+payloadData.id, payloadData, {
      headers: headers,
    })
    .then((getUpdateResult) => getUpdateResult.data)
    .catch((error) => error);

const deleteUserAPIcall = async (payloadData) =>
  await axios
    .delete(
      baseURL + "/users/"+payloadData.deleteId,
      { headers: headers }
    )
    .then((getDeleteRes) => getDeleteRes.data)
    .catch((error) => error);

/*user api call section end*/

//user list api call function start
function* getUserList({ payload }) {
  if (payload === "" || payload === undefined) {
    payload = { pageNumber: "", perPage: "" };
  }
  try {
    const getUserRes = yield call(getUserAPICallRequest, payload);
    if (getUserRes !== "") {
      yield put(getUsersSuccess(getUserRes));
    } else {
      yield put(showErrorMessage());
    }
  } catch (error) {
    yield put(showErrorMessage(error));
  }
}

function* getTodoList({ payload }) {
  if (payload === "" || payload === undefined) {
    payload = { pageNumber: "", perPage: "" };
  }
  try {
    const getTodoRes = yield call(getTodoAPICallRequest, payload);
    if (getTodoRes !== "") {
      yield put(getTodoSuccess(getTodoRes));
    } else {
      yield put(showErrorMessage());
    }
  } catch (error) {
    yield put(showErrorMessage(error));
  }
}

function* saveUser({ payload }) {
  try {
    var getSaveResult = yield call(saveUserAPICallRequest, payload);
    if (getSaveResult) {
      message.success("User added successfully");
      yield put(push("/user"));
    } else {
      message.error("Error in adding new user");
    }
  } catch (error) {
    message.error(error);
  }
}

function* updateUser({ payload }) {
  try {
    var getUpdateResult = yield call(updateUserAPICallRequest, payload);
    if (getUpdateResult) {
      message.success("User updated successfully");
      yield put(push("/user"));
    } else {
      message.error("Error in updating user");
    }
  } catch (error) {
    message.error(error);
  }
}

function* deleteUser({ payload }) {
  try {
    const getDeleteResult = yield call(deleteUserAPIcall, payload);
    if (getDeleteResult == '') {
      message.success("User deleted successfully");
      var payloadData = {
        pageNumber: 1,
        perPage: 10,
      };
      const getUserRes = yield call(getUserAPICallRequest, payloadData);
      if (getUserRes) {
        yield put(getUsersSuccess(getUserRes));
      } else {
        message.error("Error in deleting user");
      }
    } else {
      message.error("Error in deleting user");
    }
  } catch (error) {
    yield put(showErrorMessage(error));
  }
}
//user api call function end

//take every function call
export function* getUsers() {
  yield takeEvery(GET_USER, getUserList);
}
export function* getTodos() {
  yield takeEvery(GET_TODO, getTodoList);
}
export function* addUser() {
  yield takeEvery(SAVE_USER_DATA, saveUser);
}
export function* editUser() {
  yield takeEvery(UPDATE_USER_DATA, updateUser);
}
export function* removeUser() {
  yield takeEvery(DELETE_USER_DATA, deleteUser);
}
export default function* rootSaga() {
  yield all([fork(getUsers), fork(getTodos), fork(addUser), fork(editUser), fork(removeUser)]);
}
