import {all, call, fork, put, takeEvery} from "redux-saga/effects";
import axios from "axios";
import { baseURL } from "util/config";
import {
  SIGNUP_USER,
  SIGNIN_USER,
  SIGNOUT_USER
} from "constants/ActionTypes";
import {userSignUpSuccess, userSignInSuccess, userSignOutSuccess, hideAuthLoader} from "../../appRedux/actions/Auth";
import { message } from "antd";

const signInUserWithEmailPasswordRequest = async (email, password) =>
  await axios
    .post(baseURL + "/login", { email: email, password: password })
    .then((authUser) => authUser.data)
    .catch((error) => error);

const signUpUserWithEmailPasswordRequest = async (email, password, role) =>
  await axios
    .post(baseURL + "/register", { email: email, password: password, role: role })
    .then((authUser) => authUser.data)
    .catch((error) => error);

function* signUpUserWithEmailPassword({payload}) {
  const { email, password, role } = payload;
  try {
    const signUpUser = yield call(signUpUserWithEmailPasswordRequest, email, password, role);
    if (signUpUser.token !== undefined) {
      localStorage.setItem('token', signUpUser.token);
      localStorage.setItem('role', role);
      yield put(userSignUpSuccess(signUpUser.token));
    } else {
      yield put(hideAuthLoader());
      message.error("Something went wrong");
    }
  } catch (error) {
    message.error(error);
  }
}

function* signInUserWithEmailPassword({payload}) {
  const { email, password } = payload;
  try {
    const signInUser = yield call(signInUserWithEmailPasswordRequest, email, password);
    if (signInUser.token !== undefined) {
      localStorage.setItem('token', signInUser.token);
      localStorage.setItem('role', 'Admin');
      yield put(userSignInSuccess(signInUser.token));
    } else {
      yield put(hideAuthLoader());
      message.error("Invalid Email Or Password");
    }
  } catch (error) {
    message.error(error);
  }
}

function* signOut() {
  try {
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    yield put(userSignOutSuccess(signOutUser));
  } catch (error) {
    message.error(error);
  }
}

export function* signUpUser() {
  yield takeEvery(SIGNUP_USER, signUpUserWithEmailPassword);
}

export function* signInUser() {
  yield takeEvery(SIGNIN_USER, signInUserWithEmailPassword);
}

export function* signOutUser() {
  yield takeEvery(SIGNOUT_USER, signOut);
}

export default function* rootSaga() {
  yield all([
    fork(signUpUser),
    fork(signInUser),
    fork(signOutUser)
  ]);
}
