import {
    GET_USER,
    GET_USER_SUCCESS_DATA,
    GET_TODO,
    GET_TODO_SUCCESS_DATA,
    SAVE_USER_DATA,
    UPDATE_USER_DATA,
    DELETE_USER_DATA,
    GET_STATUS_INITIAL,
    SHOW_MESSAGE
  } from "../../constants/ActionTypes";
  
  export const getUsers = (user) => {
    return {
      type: GET_USER,
      payload: user
    };
  };

  export const getUsersSuccess = (data) => {
    return {
      type: GET_USER_SUCCESS_DATA,
      payload: data
    };
  };
  export const getTodo = (user) => {
    return {
      type: GET_TODO,
      payload: user
    };
  };

  export const getTodoSuccess = (data) => {
    return {
      type: GET_TODO_SUCCESS_DATA,
      payload: data
    };
  };
  export const saveUserData = (Data) => {
    return {
      type: SAVE_USER_DATA,
      payload: Data
    };
  };

  export const updateUserData = (Data) => {
    return {
      type: UPDATE_USER_DATA,
      payload: Data
    };
  };

  export const deleteUserData = (Data) => {
    return {
      type: DELETE_USER_DATA,
      payload: Data
    };
  };
  
  export const setStatusToInitial = () => {
    return {
      type: GET_STATUS_INITIAL,
    };
  };
  
  export const showErrorMessage = () => {
    return {
      type: SHOW_MESSAGE
    };
  };