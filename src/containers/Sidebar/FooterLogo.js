import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";

// import {onNavStyleChange, toggleCollapsedSideNav} from "appRedux/actions/Setting";
import {
  NAV_STYLE_DRAWER,
  NAV_STYLE_FIXED,
  NAV_STYLE_NO_HEADER_MINI_SIDEBAR,
  TAB_SIZE,
  THEME_TYPE_LITE
} from "../../constants/ThemeSetting";


const FooterLogo = () => {
  const {width, themeType} = useSelector(({settings}) => settings);

  let navStyle = useSelector(({settings}) => settings.navStyle);

  if (width < TAB_SIZE && navStyle === NAV_STYLE_FIXED) {
    navStyle = NAV_STYLE_DRAWER;
  }
  return (
    <Link to="./user" className="gx-site-logo">
      {navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR && width >= TAB_SIZE ?
        <img alt="lo" src={require("assets/images/laptop.png")}/> :
        themeType === THEME_TYPE_LITE ?
          <img alt="logo1" src={require("assets/images/laptop.png")} height="70" width="70"/> :
          <center><img alt="logo2" src={require("assets/images/laptop.png")} height="70" width="70"/></center>
      }
    </Link>
  );
};

export default FooterLogo;
