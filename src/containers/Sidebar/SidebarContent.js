import React from "react";
import {Menu} from "antd";
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import {userSignOut} from "appRedux/actions/Auth";
import CustomScrollbars from "util/CustomScrollbars";
import SidebarLogo from "./SidebarLogo";
import FooterLogo from "./FooterLogo";
import UserProfile from "./UserProfile";
import {
  // NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR,
  NAV_STYLE_NO_HEADER_MINI_SIDEBAR,
  THEME_TYPE_LITE
} from "../../constants/ThemeSetting";
import IntlMessages from "../../util/IntlMessages";
import {useSelector} from "react-redux";
import {webURL} from "util/config";

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

const SidebarContent = () => {

  const dispatch = useDispatch();
  let {navStyle, themeType, pathname} = useSelector(({settings}) => settings);

  const getNavStyleSubMenuClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR) {
      return "gx-no-header-submenu-popup";
    }
    return "";
  };
  let selectedKeys = pathname.substr(1);
  const defaultOpenKeys = selectedKeys.split('/')[2];
  
  
  if(selectedKeys === 'add-profiler'){
    selectedKeys = 'profiler';
  }
  else if(selectedKeys === 'add-staff'){
    selectedKeys = 'staff';
  }
  else if (selectedKeys === 'dashboard'){
    selectedKeys = 'dashboard'
  }
  return (
    <>
      <SidebarLogo/>
      
      <div className="gx-sidebar-content">
        <CustomScrollbars className="gx-layout-sider-scrollbar">
          <Menu
            defaultOpenKeys={[defaultOpenKeys]}
            selectedKeys={[selectedKeys]}
            theme={themeType === THEME_TYPE_LITE ? 'lite' : 'dark'}
            mode="inline">
            <Menu.Item key="user">
                  <Link to="/user">
                  <i className="icon icon-user"/>
                    <span>User</span>
                  </Link>
            </Menu.Item>

            <MenuItemGroup key="main" className="gx-menu-group">
 
               <Menu.Item key="">
                <UserProfile/>
              </Menu.Item>

            </MenuItemGroup>

          </Menu>
        </CustomScrollbars>
        <FooterLogo/>
      </div>
    </>
  );
};

SidebarContent.propTypes = {};
export default SidebarContent;

