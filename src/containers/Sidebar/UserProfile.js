import React from "react";
import {useDispatch} from "react-redux";
// import {Avatar, Popover} from "antd";
import {userSignOut} from "appRedux/actions/Auth";
import {Link} from "react-router-dom";
import IntlMessages from "../../util/IntlMessages";

const UserProfile = () => {
  const dispatch = useDispatch();

  return (
    <Link onClick={() => dispatch(userSignOut())}>
      <i className="icon icon-signin" /><span><IntlMessages id="sidebar.signout"/></span>
    </Link>
  )
};

export default UserProfile;
