import React from "react";
import IntlMessages from "util/IntlMessages";
const languageData = [
  {
    languageId: 'english',
    locale: 'en',
    name: <IntlMessages id="language.English"/>,
    icon: 'us'
  },
  {
    languageId: 'spanish',
    locale: 'es',
    name: <IntlMessages id="language.Spanish"/>,
    icon: 'es'
  },
  {
    languageId: 'catalan',
    locale: 'ct',
    name: <IntlMessages id="language.Catalan"/>,
    icon: 'ct'
  }

];
export default languageData;
