var d = new Date();
var n = d.getFullYear();
module.exports = {
  footerText: 'Copyright Demo © '+n,
  baseURL: 'https://reqres.in/api',
  baseURLJP: 'https://jsonplaceholder.typicode.com',
}