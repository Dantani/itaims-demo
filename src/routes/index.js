import React from "react";
import {Route, Switch} from "react-router-dom";

import User from "./user/index";
import TODO from "./user/todo";
const App = ({match}) => (
  <div className="gx-main-content-wrapper">
    <Switch>
      <Route path={`${match.url}user`} component={User}/>
      <Route path={`${match.url}todo-list`} component={TODO}/>
    </Switch>
  </div>
);

export default App;
