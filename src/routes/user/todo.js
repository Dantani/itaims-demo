import React, { Component } from "react";
import { Card, Divider, Table, Button, Col, Row, Input, message, Form, Modal } from "antd";
import { connect } from "react-redux";
import DateWithoutTimeHelper from "../helper/DateWithoutTimeHelper";
import IntlMessages from "util/IntlMessages"
import {
  getTodo,
  setStatusToInitial,
} from "appRedux/actions/UserActions";
import CircularProgress from "components/CircularProgress/index";
import {FormattedMessage, injectIntl} from "react-intl";
import { Link } from "react-router-dom";

const FormItem = Form.Item;
var userId = '';

class Todo extends Component {
  constructor() {
    super();
    this.state = {
      pagination: {
        pageSize: 10,
        showSizeChanger: true,
        pageSizeOptions: ['10', '20', '30', '40'],
      },
      addUserModal: false,
      modalDeleteVisible: false,
      Id: '',
      user_name: '',
      editUserFlag: '',
      delete_id: '',
      searchedUser: '',
      passUserData: [],
    }
  }

  getUserListing(page, perPage, userId) {
    if (this.props.status == 'Initial') {
      this.props.getTodo({'pageNumber': 1, 'perPage': perPage, 'userId': userId});
    }
    else {
      if (page === '') {
        page = 1;
      }
      if (perPage === '') {
        perPage = '10';
      }
      this.props.getTodo({'pageNumber': page, 'perPage': perPage,'userId': userId});
    }
  }

  componentDidMount() {
    if (this.props.location.state) {
      const userDataArray = this.props.location.state.passUserData;
      console.log("PASSED DATA =>", userDataArray);
        if (userDataArray.length !== 0) {
          userId = userDataArray;
          this.props.setStatusToInitial();
          this.getUserListing(1, 10, userId);
        }
    }
  }

  handleGoBack = () => {
    this.props.history.push({
      pathname: "/user",
    });
  };

  handleTableChange = (pagination, filters, sorter) => {
    const pager = { ...this.state.pagination };
    pager.current = pagination.current;
    pager.pageSize = pagination.pageSize;
    this.setState({
      pagination: pager,
    });
    this.getUserListing(pagination.current, pagination.pageSize, userId);
  };

  handleUserSubmit = (e) => {
    e.preventDefault();
    var userData = [];
    this.props.form.validateFieldsAndScroll(['userName'], (err, values) => {
      if (!err) {
        if (this.state.editUserFlag === 'edit') {
          userData['Id'] = this.state.Id;
          userData['Name'] = this.state.user_name;
          userData['UserId'] = '';

          var user_id = this.state.Id;
          var user_name = this.state.user_name;

          if (user_id !== '' && user_name !== '') {
            this.setState({ addUserModal: false });
            // this.props.saveUserData(userData);
          }
          else {
            message.error(this.props.intl.formatMessage({id: 'global.TryAgain'}));
          }
        }
        else {
          userData['Name'] = this.state.user_name;
          userData['UserId'] = '';

          var user_name = this.state.user_name;

          if (user_name !== '' && '' !== '') {
            this.setState({ addUserModal: false });
            // this.props.saveUserData(userData);
          }
          else {
            message.error(this.props.intl.formatMessage({id: 'global.TryAgain'}));
          }
        }
      }
    })
  };

  onAddUser = () => {
    this.setState({'user_name': ''});
    this.setState({ addUserModal: true });
    this.setState({ editUserFlag: '' });
  };

  onEditUser = (uid) => {
    var userData = this.props.getUserData.data.find((singleUser) => {
      return singleUser.id === uid;
    })
    this.props.history.push({
      pathname: "/add-user",
      state: { passUserData: userData },
    });
  };


  closeAddUser = () => {
    this.setState({ addUserModal: false });
  };

  onDeleteUser = (deleteId) => {
    this.setState({ modalDeleteVisible: true });
    this.setState({ delete_id: deleteId });
  };

  confirmDelete = () => {
    this.props.deleteUserData({'deleteId': this.state.delete_id});
    this.setState({ modalDeleteVisible: false });
  }

  cancelDelete = (e) => {
    this.setState({ delete_id: '' });
    this.setState({ modalDeleteVisible: false });
  }

  render() {
    var usersData = this.props.getTodoData;
    var userData = '';

    if (!usersData) {
      // Object is empty (Would return true in this example)
    }
    else {
      userData = usersData;

      const pagination = { ...this.state.pagination };
      var old_pagination_total = pagination.total;

      pagination.total = usersData.total;
      pagination.current = this.state.pagination.current ? this.state.pagination.current : 1;

      var end_record = '';
      if (pagination.current === 1) {
        end_record = pagination.pageSize;
      }
      else {
        end_record = pagination.current * pagination.pageSize;
        if (end_record > pagination.total) {
          end_record = pagination.total;
        }
      }

      if (pagination.current !== '' && this.state.pagination.current === undefined) {
        this.setState({
          pagination
        });
      }
      else if (pagination.total !== '' && pagination.total !== old_pagination_total) {
        pagination.current = 1;
        this.setState({
          pagination
        });
      }
      else if ((pagination.total === '' || pagination.total === 0) && pagination.total !== old_pagination_total) {
        this.setState({
          pagination
        });
      }
    }

    const columns = [
      {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
        sorter: false,
        render: text => <span className="">{text}</span>,
      },
      {
        title: 'User Id',
        dataIndex: 'userId',
        key: 'userid',
        sorter: false,
        render: text => <span className="">{text}</span>,
      },
      // {
      //   title: 'Profile',
      //   key: 'avatar',
      //   render: (text, record) => <img src={`${record.avatar}`} width="50" height="50" />,
      // },
      {
        title: 'Title',
        dataIndex: 'title',
        key: 'title',
        sorter: false,
        render: text => <span className="">{text}</span>,
      },
      {
        title: 'Completed',
        dataIndex: 'completed',
        key: 'completed',
        sorter: false,
        render: text => <span className="">{text?"True":"False"}</span>,
      }
    ];

    const { getFieldDecorator } = this.props.form;
    return (
      <Card className="custo_head_wrap" title="Todo List" extra={<div className="card-extra-form">
        <Link to={{pathname: "add-user", state: { passUserData: [] }}}>
          {/* <Button className="gx-mb-0" type="primary" style={{ float: "right" }}>Add User</Button> */}
        </Link>

      </div>}>

        <Table className="gx-table-responsive" columns={columns} dataSource={userData} onChange={this.handleTableChange} pagination={this.state.pagination}/>
        <Modal
          title={this.state.editUserFlag === 'edit' ? <IntlMessages id="userEdit.editUser" /> : <IntlMessages id="userAdd.addUser" />}
          maskClosable={false}
          onCancel={this.closeAddUser}
          visible={this.state.addUserModal}
          closable={true}
          okText={<IntlMessages id="additentity.save" />}
          cancelText={<IntlMessages id="globalButton.cancel" />}
          onOk={this.handleUserSubmit}
          destroyOnClose={true}
          className="cust-modal-width">
          <div className="gx-modal-box-row">                              
            <div className="gx-modal-box-form-item">
              <Form>
                <div className="gx-form-group">
                  <Row>
                    <Col lg={24} xs={24}>
                      {/* <lable><sup><span style={{color: "red", fontSize: "10px"}}>*</span></sup> <IntlMessages id="userAdd.userName"/> :</lable> */}
                      <FormattedMessage id="placeholder.userName">
                        {placeholder =>
                          <FormItem>
                            {getFieldDecorator('userName', {
                              initialValue: this.state.user_name,
                              rules: [{
                                required: true,
                                message: <IntlMessages id="required.userAdd.userName" />,
                                whitespace: true
                              }],
                            })(
                              <Input
                                required
                                placeholder={placeholder}
                                onChange={(event) => this.setState({ user_name: event.target.value })}
                                margin="none" />
                            )}
                          </FormItem>
                        }
                      </FormattedMessage>
                    </Col>
                  </Row>
                </div>
              </Form>
            </div>
          </div>
        </Modal>
        <Modal className=""
          title="Delete User"
          visible={this.state.modalDeleteVisible}
          destroyOnClose={true}
          onCancel={() => this.cancelDelete()}
          onOk={() => this.confirmDelete()}
          okText="Delete"
          cancelText="Cancel"
        >
          <div className="gx-modal-box-row">
            <div className="gx-modal-box-form-item">
              <div className="mail-successbox">
                <h4 className="err-text">Are you sure want to delete user?</h4>
              </div>
            </div>
          </div>
        </Modal>
        {this.state.loader || this.props.loader ?
          <div className="gx-loader-view">
            <CircularProgress />
          </div> :
          null
        }
      </Card>
    );
  }
}

// Object of action creators
const mapDispatchToProps = {
  getTodo,
  setStatusToInitial,
}

const viewUserReportForm = Form.create()(Todo);

const mapStateToProps = state => {
  return {
    getTodoData: state.userReducers.get_todo_res,
    loader: state.userReducers.loader,
    status: state.userReducers.status,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(viewUserReportForm));